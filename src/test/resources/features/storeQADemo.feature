Feature: Selenium challenge

  Scenario: Submit a comment with a wrong e-mail
    Given Open the page
    When Click in samples button
    And the page should be samples page
    And enter "Hi, this is a tryout" on comment text area
    And enter "Sergio" on name text area
    And enter "wrongemail" on email text area
    And enter "google.es" on url text area
    And submit the comment
    Then an error should be displayed
    And press the back link
    And the page should be samples page
    And enter "sergiomoranreina@gmail.com" on email text area
    And submit the comment
    And the comment should appear in sended messages list


