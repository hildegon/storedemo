package features.stepDefinition;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import features.seleniumSteps.StoreQASteps;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;

public class StoreQAStepsDefinition {

    @Steps
    private static StoreQASteps storeSearchSteps;


    @Given("^Open the page$")
    public static void openThePage() {
        storeSearchSteps.openStorePage();
    }

    @Then("^Click in samples button$")
    public static void clickInSamplesButton() {
        storeSearchSteps.clickSamplesButton();
    }

    @And("^enter \"([^\"]*)\" on (name|comment|email|url) text area$")
    public static void enterOnNameTextArea(String text, String textArea) {
        switch (textArea) {
            case "name":
                storeSearchSteps.enterName(text);
                break;
            case "comment":
                storeSearchSteps.enterComment(text);
                break;
            case "email":
                storeSearchSteps.enterEmail(text);
                break;
            case "url":
                storeSearchSteps.enterUrl(text);
            default:
                break;
        }

    }

    @And("^submit the comment$")
    public static void submitTheComment() {
        storeSearchSteps.clickSubmitButton();
    }

    @Then("^an error should be displayed$")
    public static void anErrorShouldBeDisplayed() {
        Assert.assertTrue("Error page is shown", storeSearchSteps.errPageShown());
    }

    @And("^press the back link$")
    public static void pressTheBackLink() {
        storeSearchSteps.errPageBack();
    }

    @And("^the page should be samples page$")
    public void thePageShouldBeSamplesPage() {
        Assert.assertTrue("Samples page is shown", storeSearchSteps.samplesPageShown());
    }

    @And("^the comment should appear in sended messages list$")
    public void theCommentShouldAppearInSendedMessagesList() {
        Assert.assertTrue("Message was shown in the comments lists", storeSearchSteps.messageIsDisplayed());
    }

}
