package features.seleniumSteps;

import features.errorPage;
import features.homePage;
import features.samplesPage;
import net.thucydides.core.annotations.Step;

import java.util.Date;

public class StoreQASteps {

    private homePage homePage;
    private samplesPage samplesPage;
    private errorPage errorPage;


    @Step
    public void openStorePage() {
        homePage.open();
    }

    @Step
    public void clickSamplesButton() {
        homePage.clickSamplesButton();
    }

    @Step
    public void enterName(String name) {
        samplesPage.enterName(name);
    }

    @Step
    public void enterComment(String comment) {
        Date a = new Date();
        comment = comment + a.getTime();
        samplesPage.enterComment(comment);
    }

    @Step
    public void enterEmail(String email) {
        samplesPage.enterEmail(email);
    }

    @Step
    public void enterUrl(String url) {
        samplesPage.enterUrl(url);
    }

    @Step
    public void clickSubmitButton() {
        samplesPage.clickSubmitButton();
    }

    @Step
    public boolean errPageShown() {
        return errorPage.errPageShown();
    }

    @Step
    public void errPageBack() {
        errorPage.errPageBack();
    }

    @Step
    public boolean samplesPageShown() {
        return samplesPage.samplesPageIsShown();
    }

    @Step
    public boolean messageIsDisplayed() {
        return samplesPage.commentIsDisplayed();
    }
}
