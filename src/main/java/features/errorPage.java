package features;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class errorPage extends PageObject {


    @FindBy(id = "error-page")
    private WebElement err_page;

    @FindBy(css = "#error-page > p:nth-child(4) > a")
    private WebElement href_back;

    public boolean errPageShown() {
        Boolean isDisplayed = false;
        if (err_page.isDisplayed()){
            isDisplayed = true;
        }
        return isDisplayed;
    }

    public void errPageBack() {
        href_back.click();
    }
}
