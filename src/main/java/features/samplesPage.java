package features;

import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

public class samplesPage extends PageObject {

    private String commentario;

    @FindBy(id = "comment")
    private WebElement ta_comment;

    @FindBy(id = "author")
    private WebElement ta_author;

    @FindBy(id = "email")
    private WebElement ta_email;

    @FindBy(id = "url")
    private WebElement ta_url;

    @FindBy(id = "submit")
    private WebElement bt_submit;

    @FindBy(className = "entry-title")
    private WebElement tx_samplesTitle;

    @FindBy(className = "commentlist")
    private WebElement ol_commentTable;


    public void enterName(String name) {

        ta_author.click();
        ta_author.sendKeys(name);

    }

    public void enterComment(String comment) {
        commentario = comment;
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView();", ta_comment);
        ta_comment.click();
        ta_comment.sendKeys(comment);

    }

    public void enterEmail(String email) {

        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView();", ta_email);
        ta_email.click();
        ta_email.sendKeys(email);

    }

    public void enterUrl(String url) {

        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView();", ta_url);
        ta_url.click();
        ta_url.sendKeys(url);

    }

    public void clickSubmitButton() {
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView();", bt_submit);
        bt_submit.click();

    }

    public boolean samplesPageIsShown() {
        Boolean isDisplayed = false;
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView();", tx_samplesTitle);
        if (tx_samplesTitle.getText().equals("Sample Page")) {
            isDisplayed = true;
        }
        return isDisplayed;
    }

    public boolean commentIsDisplayed() {
        Boolean isInModeration = false;
        getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView();", ol_commentTable);
        for (WebElement li_comment : ol_commentTable.findElements(By.tagName("li"))) {
            if (li_comment.getText().contains(commentario)) {
                isInModeration = true;
            }
        }
        return isInModeration;
    }
}
