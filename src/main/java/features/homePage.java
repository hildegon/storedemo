package features;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.WhenPageOpens;
import net.thucydides.core.pages.PageObject;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("http://store.demoqa.com/")
public class homePage extends PageObject {

    @FindBy(id = "menu-item-54")
    private WebElement bt_samples;

    @WhenPageOpens
    public void waitForLoad() {
        getDriver().manage().window().maximize();
        ((JavascriptExecutor) getDriver()).executeScript("scroll(0,600)");

    }

    public homePage(WebDriver driver) {
        super(driver);
    }


    public void clickSamplesButton() {
        bt_samples.click();
    }

}

